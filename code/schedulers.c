#include "scheduler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int op1()
{
	FILE* stream = fopen("data/dataset1.csv", "r");

	String data[6][3];
	
	int x = 0;	
	char line[1024];
	while (fgets(line, 1024, stream))
	{
        	char* tmp = strdup(line);
        	printf("Field 3 would be %s\n", getfield(tmp, 3));
		data[x][0] = strdup(getfield(tmp,1)) - '0';
		data[x][1] = strdup(getfield(tmp,2)) - '0';
		data[x][2] = strdup(getfield(tmp,2)) - '0';
        	// NOTE strtok clobbers tmp
		x = x+ 1;
        	free(tmp);
    	}
	printf("%d %d %d\n", data[0][0],data[0][1],data[0][2]);
	printf("%d %d %d\n", data[1][0],data[1][1],data[1][2]);
	printf("%d %d %d\n", data[2][0],data[2][1],data[2][2]);
	printf("%d %d %d\n", data[3][0],data[3][1],data[3][2]);
	printf("%d %d %d\n", data[4][0],data[4][1],data[4][2]);
	printf("%d %d %d\n", data[5][0],data[5][1],data[5][2]);
	return 1;
}

int op2()
{
	return 2;
}

int op3()
{
	return 3;
}

const char* getfield(char* line, int num)
{
	const char* tok;
	for (tok = strtok(line, ",");
            	tok && *tok;
            	tok = strtok(NULL, ",\n"))
    	{
        	if (!--num)
            	return tok;
    	}
    	return NULL;
}
