#include "scheduler.h"
#include <stdlib.h>
#include <stdio.h>

void waitEnter()
{
	int ch;
	while ((ch = getchar()) != '\n' && ch != EOF);
	getchar();
}

int main()
{
	int input = 0;

	while( input != 4)
	{
		system("clear");
		printf("1 - First Come First Served (FCFS) Scheduler\n");
		printf("2 - Shortest Job First (SJF) Scheduler\n");
		printf("3 - Round Robin (RR) Scheduler\n");
		printf("4 - quit\n");
		printf("Enter a Selection: ");

		scanf("%d", &input);
		if (input == 1)
		{
			printf("%d\n",op1());
			waitEnter();
		}
		else if (input == 2)
		{
			printf("%d\n",op2());
			waitEnter();
		}
		else if (input == 3)
		{
			printf("%d\n",op3());
			waitEnter();
		}
		else if (input == 4)
		{

		}
		else
		{
			printf("Invalid input\n");
			waitEnter();
		}
	}
	system("clear");
	return 0;
}
